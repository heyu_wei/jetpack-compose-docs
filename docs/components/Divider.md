设置分割线

``` kotlin
@Composable
fun ColumnSample() {
    Column(
        modifier = Modifier
            .requiredHeight(100.dp)
            .background(Color.White),
    ) {
        Text(
            text = "First ", modifier = Modifier
                .background(Color.Red)
                .weight(0.7f,fill=false)
        )
        
				Divider()
				
        Text("Second", modifier = Modifier
            .background(Color.Green)
            .weight(0.3f))
    }
}
```

## 视频教程

<a href="https://www.bilibili.com/video/BV1PZ4y1k7Ar/" target="_blank">https://www.bilibili.com/video/BV1PZ4y1k7Ar/</a>

<a href="https://www.bilibili.com/video/BV1aS4y1D7dv/">实战视频</a>
