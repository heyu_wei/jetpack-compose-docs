空白区域，通过 modifier 设置空白区域的大小

``` kotlin
@Composable
fun SpacerSample() {
    Column(modifier =  Modifier.background(Color.White)) {
        Row(modifier = Modifier.size(100.dp)) {
            Text("First", modifier = Modifier.background(Color.Red))

            Spacer(modifier = Modifier.weight(1f))

            Text("Second", modifier = Modifier.background(Color.Green))
        }

        Divider()

        Text("Column Item 0", modifier = Modifier.background(Color.Red))

        Spacer(modifier = Modifier.weight(1f))

        Text("Column Item 1", modifier = Modifier.background(Color.Green))
    }
}
```

![Spacer](../assets/Spacer.png)

## 视频教程

<a href="https://www.bilibili.com/video/BV1rF411E7Kn/" target="_blank">https://www.bilibili.com/video/BV1rF411E7Kn/</a>

<a href="https://www.bilibili.com/video/BV1aS4y1D7dv/">实战视频</a>