``` kotlin
@Composable
fun Checkbox(
    checked: Boolean,
    onCheckedChange: ((Boolean) -> Unit)?,
    modifier: Modifier = Modifier,
    enabled: Boolean = true,
    interactionSource: MutableInteractionSource = remember { MutableInteractionSource() },
    colors: CheckboxColors = CheckboxDefaults.colors()
)
```

- checked 是否选中
- onCheckedChange 选择回调
- enabled 是否启用



``` kotlin
@Composable
fun CheckBoxSample() {
    //接收选中状态
    var checkedList by remember {
        mutableStateOf(
            listOf(false, false)
        )
    }
    Column {
        checkedList.forEachIndexed { i, item ->
            Checkbox(checked = item, onCheckedChange = {
                checkedList = checkedList.mapIndexed { j, b ->
                    if (i == j) {
                        !b
                    } else {
                        b
                    }
                }
            })
        }
    }
}
```

![CheckBox](../assets/CheckBox.gif)

## 视频教程

<a href="https://www.bilibili.com/video/BV1ZP4y1u758/" target="_blank">https://www.bilibili.com/video/BV1ZP4y1u758/</a>

<a href="https://www.bilibili.com/video/BV1aS4y1D7dv/">实战视频</a>