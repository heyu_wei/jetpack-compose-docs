``` kotlin
@Composable
fun RadioButton(
    selected: Boolean,
    onClick: (() -> Unit)?,
    modifier: Modifier = Modifier,
    enabled: Boolean = true,
    interactionSource: MutableInteractionSource = remember { MutableInteractionSource() },
    colors: RadioButtonColors = RadioButtonDefaults.colors()
):Unit
```

- checked 是否选中
- onCheckedChange 选择回调
- enabled 是否启用

``` kotlin
@Composable
fun RadioButtonSample1() {
    var checkedList by remember { mutableStateOf(listOf(false, false)) }
    LazyColumn() {
        items(checkedList.size) { i ->
            RadioButton(selected = checkedList[i], onClick = {
                checkedList = checkedList.mapIndexed { j, _ ->
                    i == j
                }
            })
        }
    }
}
```

![RadioButton](../assets/RadioButton.gif)

## 视频教程

<a href="https://www.bilibili.com/video/BV1xq4y1475H" target="_blank">https://www.bilibili.com/video/BV1xq4y1475H</a>

<a href="https://www.bilibili.com/video/BV1aS4y1D7dv/">实战视频</a>