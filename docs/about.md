# 关于

## 关于内容

本内容是集于网络，大部分内容来自官方文档以及阅读源码得来的结果，也有部分是看了一些大佬的视频和文章，如果内容有任何侵权问题，请联系我进行删除处理。

## 找到组织

![QQ](assets/qq.png)

QQ群:<a href="https://qm.qq.com/cgi-bin/qm/qr?k=v1a2ml66bujPQrppjSrB2wz1O_ZDh9Lc&jump_from=webapi" target="_blank">743346044</a>


## 关于作者

作者还是每天忙于工作的小码农

### Git

<a href="https://github.com/RandyWei">GitHub</a>

<a href="https://gitee.com/RandyWei">Gitee</a>

## 视频教程

- <a href="https://www.bilibili.com/video/BV1oq4y147zJ/">基础姿势</a>
- <a href="https://www.bilibili.com/video/BV1aS4y1D7dv/">实战视频</a>
