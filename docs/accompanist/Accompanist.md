**Accompanist ：Jetpack Compose 的工具包**

## 官方地址

<a href="https://google.github.io/accompanist/" target="_blank">https://google.github.io/accompanist/</a>

<a href="https://github.com/google/accompanist" target="_blank">https://github.com/google/accompanist</a>

[![Maven Central](https://img.shields.io/maven-central/v/com.google.accompanist/accompanist-insets)](https://search.maven.org/search?q=g:com.google.accompanist)

## 功能

### Insets 

设置 <a href="https://developer.android.com/reference/kotlin/android/view/WindowInsets" target="_blank">WindowsInsets</a>


``` groovy title="配置"
repositories {
    mavenCentral()
}

dependencies {
    implementation "com.google.accompanist:accompanist-insets:<version>"
    // If using insets-ui
    implementation "com.google.accompanist:accompanist-insets-ui:<version>"
}
```

### System UI Controller

可以设置系统状态栏的颜色和显示隐藏

``` groovy title="配置"
repositories {
    mavenCentral()
}

dependencies {
    implementation "com.google.accompanist:accompanist-systemuicontroller:<version>"
}
```

### AppCompat Theme Adapter

引用 AppCompat 的 xml 形式主题

``` groovy title="配置"
repositories {
    mavenCentral()
}

dependencies {
    implementation "com.google.accompanist:accompanist-appcompat-theme:<version>"
}
```

### Pager

类似 Android 里面的 ViewPager

``` groovy title="配置"
repositories {
    mavenCentral()
}

dependencies {
    implementation "com.google.accompanist:accompanist-pager:<version>"

    // If using indicators, also depend on 
    implementation "com.google.accompanist:accompanist-pager-indicators:<version>"
}
```

### Permissions

权限请求

``` groovy title="配置"
repositories {
    mavenCentral()
}

dependencies {
    implementation "com.google.accompanist:accompanist-permissions:<version>"
}
```

### Placeholder

更简单的使用数据未加载时的占位符

``` groovy title="配置"
repositories {
    mavenCentral()
}

dependencies {
    // If you're using Material, use accompanist-placeholder-material
    implementation "com.google.accompanist:accompanist-placeholder-material:<version>"

    // Otherwise use the foundation version
    implementation "com.google.accompanist:accompanist-placeholder:<version>"
}
```

### Flow Layouts

Flexbox-line 布局

``` groovy title="配置"
repositories {
    mavenCentral()
}

dependencies {
    implementation "com.google.accompanist:accompanist-flowlayout:<version>"
}
```

### Navigation-Animation

导航动画

``` groovy title="配置"
repositories {
    mavenCentral()
}

dependencies {
    implementation "com.google.accompanist:accompanist-navigation-animation:<version>"
}
```

### Navigation-Material

提供<a href="https://developer.android.com/jetpack/androidx/releases/compose-material" target="_blank">Compose Material</a> 支持，比如 model bottom sheets

``` groovy title="配置"
repositories {
    mavenCentral()
}

dependencies {
    implementation "com.google.accompanist:accompanist-navigation-material:<version>"
}
```

### Drawable Painter

更灵活的使用 Android Drabables

``` groovy title="配置"
repositories {
    mavenCentral()
}

dependencies {
    implementation "com.google.accompanist:accompanist-drawablepainter:<version>"
}
```

### Swipe to Refresh

下拉刷新，类似 Android 的 SwipeRefreshLayout

``` groovy title="配置"
repositories {
    mavenCentral()
}

dependencies {
    implementation "com.google.accompanist:accompanist-swiperefresh:<version>"
}
```

### WebView

repositories {
    mavenCentral()
}

dependencies {
    implementation "com.google.accompanist:accompanist-webview:<version>"
}

## 视频教程

- <a href="https://www.bilibili.com/video/BV1v3411W7Di/">https://www.bilibili.com/video/BV1v3411W7Di/</a>
- <a href="https://www.bilibili.com/video/BV1aS4y1D7dv/">实战视频</a>
