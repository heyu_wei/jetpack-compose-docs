## 操作


<table width=100% >

    <tr><td>
    
    作用域：任意
    
    </td>
    <td>

    ``` kotlin
    Modifier.clickable( 
        enabled: Boolean,
        onClickLabel: String?,
        role: Role?,
        onClick: () -> Unit
    )
    ```
    
    将组件配置为通过输入或无障碍“点击”事件接收点击操作。
    </td></tr>

    <tr><td>作用域：任意</td>
    <td>
    ``` kotlin
    Modifier.clickable(
        interactionSource: MutableInteractionSource,
        indication: Indication?,
        enabled: Boolean,
        onClickLabel: String?,
        role: Role?,
        onClick: () -> Unit
    )
    ```
    将组件配置为通过输入或无障碍“点击”事件接收点击操作。

    </td></tr>

    <tr><td>

    作用域：任意 
    @ExperimentalFoundationApi

    </td>
    <td>
    ``` kotlin
    Modifier.combinedClickable(
        enabled: Boolean,
        onClickLabel: String?,
        role: Role?,
        onLongClickLabel: String?,
        onLongClick: () -> Unit,
        onDoubleClick: () -> Unit,
        onClick: () -> Unit
    )
    ```
    将组件配置为通过输入或无障碍“点击”事件接收点击、双击和长按操作。

    </td></tr>

    <tr><td>

    作用域：任意 
    @ExperimentalFoundationApi

    </td>
    <td>
    ``` kotlin
    Modifier.combinedClickable(
        interactionSource: MutableInteractionSource,
        indication: Indication?,
        enabled: Boolean,
        onClickLabel: String?,
        role: Role?,
        onLongClickLabel: String?,
        onLongClick: () -> Unit,
        onDoubleClick: () -> Unit,
        onClick: () -> Unit
    )
    ```
    将组件配置为通过输入或无障碍“点击”事件接收点击、双击和长按操作。

    </td></tr>

    <tr><td>

    作用域：任意 

    </td>
    <td>
    ``` kotlin
    Modifier.draggable(
        state: DraggableState,
        orientation: Orientation,
        enabled: Boolean,
        interactionSource: MutableInteractionSource?,
        startDragImmediately: Boolean,
        onDragStarted: suspend CoroutineScope.(Offset) -> Unit,
        onDragStopped: suspend CoroutineScope.(Float) -> Unit,
        reverseDirection: Boolean
    )
    ```
    为界面元素配置单个 Orientation 的触摸拖动。

    </td></tr>


    <tr><td>

    作用域：任意 

    </td>
    <td>
    ``` kotlin
    Modifier.selectable(
        selected: Boolean,
        enabled: Boolean,
        role: Role?,
        onClick: () -> Unit
    )
    ```
    将组件配置为可选择，通常作为互斥组的一部分，在任何时间点只能选择该组中的一项。

    </td></tr>

    <tr><td>

    作用域：任意 

    </td>
    <td>
    ``` kotlin
    Modifier.selectable(
        selected: Boolean,
        interactionSource: MutableInteractionSource,
        indication: Indication?,
        enabled: Boolean,
        role: Role?,
        onClick: () -> Unit
    )
    ```
    将组件配置为可选择，通常作为互斥组的一部分，在任何时间点只能选择该组中的一项。

    </td></tr>

    <tr><td>

    作用域：任意 

    </td>
    <td>
    ``` kotlin
    Modifier.selectableGroup()
    ```
    使用此修饰符将用于实现无障碍功能的一系列 selectable 项（例如 Tab 或 RadioButton）归到一组。

    </td></tr>

    <tr><td>

    作用域：任意 

    @ExperimentalMaterialApi 
    </td>
    <td>
    ``` kotlin
        
    <T : Any?> Modifier.swipeable(
        state: SwipeableState<T>,
        anchors: Map<Float, T>,
        orientation: Orientation,
        enabled: Boolean,
        reverseDirection: Boolean,
        interactionSource: MutableInteractionSource?,
        thresholds: (from, to) -> ThresholdConfig,
        resistance: ResistanceConfig?,
        velocityThreshold: Dp
    )
    ```
    在一组预定义状态之间启用滑动手势。
    </td></tr>

    <tr><td>

    作用域：任意 

    </td>
    <td>
    ``` kotlin
    Modifier.toggleable(
        value: Boolean,
        enabled: Boolean,
        role: Role?,
        onValueChange: (Boolean) -> Unit
    )
    ```
    将组件配置为可通过输入和无障碍事件切换
    </td></tr>

    <tr><td>

    作用域：任意 

    </td>
    <td>
    ``` kotlin
        
    Modifier.toggleable(
        value: Boolean,
        interactionSource: MutableInteractionSource,
        indication: Indication?,
        enabled: Boolean,
        role: Role?,
        onValueChange: (Boolean) -> Unit
    )
    ```
    将组件配置为可通过输入和无障碍事件切换。
    </td></tr>

    <tr><td>

    作用域：任意 

    </td>
    <td>
    ``` kotlin
    Modifier.triStateToggleable(
        state: ToggleableState,
        enabled: Boolean,
        role: Role?,
        onClick: () -> Unit
    )
    ```
    将组件配置为可通过输入和无障碍事件在三种状态之间切换：启用、停用和不确定。
    </td></tr>

    <tr><td>

    作用域：任意 

    </td>
    <td>
    ``` kotlin
        
    Modifier.triStateToggleable(
        state: ToggleableState,
        interactionSource: MutableInteractionSource,
        indication: Indication?,
        enabled: Boolean,
        role: Role?,
        onClick: () -> Unit
    )
    ```
    将组件配置为可通过输入和无障碍事件在三种状态之间切换：启用、停用和不确定。
    </td></tr>
</table>

## 对齐方式

<table>

<tr><td>

作用域：BoxScope
@Stable 

</td>
<td>
``` kotlin
Modifier.align(alignment: Alignment)
```
将内容元素拉取到 Box 中的特定 Alignment。
</td></tr>

<tr><td>

作用域：RowScope
@Stable 

</td>
<td>
``` kotlin
Modifier.align(alignment: Alignment.Vertical)
```
在 Row 中垂直对齐元素。
</td></tr>

<tr><td>

作用域：RowScope
@Stable 

</td>
<td>
``` kotlin
Modifier.alignBy(alignmentLine: HorizontalAlignmentLine)
```
垂直放置元素，使其 alignmentLine 与同样配置为 alignBy 的同级元素对齐。
</td></tr>

<tr><td>

作用域： RowScope
@Stable 

</td>
<td>
``` kotlin
Modifier.alignBy(alignmentLineBlock: (Measured) -> Int)
```
垂直放置元素，使由 alignmentLineBlock 确定的内容的对齐线与同样配置为 alignBy 的同级元素对齐。
</td></tr>

<tr><td>

作用域： RowScope
@Stable  

</td>
<td>
``` kotlin
Modifier.alignByBaseline()
```
垂直放置元素，使其第一条基线与同样配置为 alignByBaseline 或 alignBy 的同级元素对齐。
</td></tr>

<tr><td>

作用域：ColumnScope
@Stable 

</td>
<td>
``` kotlin
Modifier.alignBy(alignmentLine: VerticalAlignmentLine)
```
水平放置元素，使其 alignmentLine 与同样配置为 alignBy 的同级元素对齐。
</td></tr>

<tr><td>

作用域：ColumnScope
@Stable 

</td>
<td>
``` kotlin
Modifier.alignBy(alignmentLineBlock: (Measured) -> Int)
```
水平放置元素，使由 alignmentLineBlock 确定的内容的对齐线与同样配置为 alignBy 的同级元素对齐。
</td></tr>

</table>

## 动画

<table>


<tr><td>

作用域：
AnimatedVisibilityScope
open

</td>
<td>
``` kotlin
Modifier.animateEnterExit(
    enter: EnterTransition, 
    exit: ExitTransition)
```
animateEnterExit 修饰符可用于 AnimatedVisibility 的任何直接或间接子项，以创建与 AnimatedVisibility 中指定的内容不同的进入/退出动画。
</td>
</tr>

</table>

## 边框

<table>

<tr><td>

作用域：任意 

</td>
<td>
``` kotlin
Modifier.border(border: BorderStroke, shape: Shape)
```
修改元素，以添加使用 border 和 shape 指定了外观的边框，并进行裁剪。
</td></tr>

<tr><td>

作用域：任意 

</td>
<td>
``` kotlin
Modifier.border(width: Dp, color: Color, shape: Shape)
```
修改元素，以添加使用 width、color 和 shape 指定了外观的边框，并进行裁剪。
</td></tr>

<tr><td>

作用域：任意 

</td>
<td>
``` kotlin
Modifier.border(width: Dp, brush: Brush, shape: Shape)
```
修改元素，以添加使用 width、brush 和 shape 指定了外观的边框，并进行裁剪。
</td></tr>

</table>

## 绘图

<table>


<tr><td>

作用域：任意 

@Stable 
</td>
<td>
``` kotlin
Modifier.alpha(alpha: Float)
```

使用可能小于 1 的修饰的 alpha 绘制内容。
</td></tr>

<tr><td>

作用域：任意 

</td>
<td>
``` kotlin
Modifier.background(color: Color, shape: Shape)
```

使用纯 color 在内容后方绘制 shape。
</td></tr>

<tr><td>

作用域：任意 

</td>
<td>
``` kotlin
Modifier.background(brush: Brush, shape: Shape, alpha: Float)
```

使用 brush 在内容后方绘制 shape。
</td></tr>

<tr><td>

作用域：任意 

@Stable 
</td>
<td>
``` kotlin
Modifier.clip(shape: Shape)
```

将内容裁剪到 shape。
</td></tr>

<tr><td>

作用域：任意 

@Stable 
</td>
<td>
``` kotlin
Modifier.clipToBounds()
```

将内容裁剪到此修饰符定义的图层的边界。
</td></tr>

<tr><td>

作用域：任意 

</td>
<td>
``` kotlin
Modifier.drawBehind(onDraw: DrawScope.() -> Unit)
```

绘制到修饰的内容后方的 Canvas 中。
</td></tr>

<tr><td>

作用域：任意 

</td>
<td>
``` kotlin
Modifier.drawWithCache(
    onBuildDrawCache: CacheDrawScope.() -> DrawResult
)
```

绘制到 DrawScope 中，只要绘制区域的大小不变或读取的任何状态对象未发生变化，便使内容在各绘制调用中始终保留。
</td></tr>

<tr><td>

作用域：任意 

</td>
<td>
``` kotlin
Modifier.drawWithContent(onDraw: ContentDrawScope.() -> Unit)
```

创建 DrawModifier，允许开发者在布局内容前后进行绘制。
</td></tr>

<tr><td>

作用域：任意 

</td>
<td>
``` kotlin
Modifier.indication(
    interactionSource: InteractionSource,
    indication: Indication?
)
```

在发生互动时为此组件绘制视觉效果。
</td></tr>

<tr><td>

作用域：任意 

</td>
<td>
``` kotlin
Modifier.paint(
    painter: Painter,
    sizeToIntrinsics: Boolean,
    alignment: Alignment,
    contentScale: ContentScale,
    alpha: Float,
    colorFilter: ColorFilter?
)
```

使用 painter 绘制内容。
</td></tr>

<tr><td>

作用域：任意 

</td>
<td>
``` kotlin
Modifier.shadow(elevation: Dp, shape: Shape, clip: Boolean)
```

创建用于绘制阴影的 GraphicsLayerModifier。
</td></tr>

<tr><td>

作用域：任意 

</td>
<td>
``` kotlin
Modifier.zIndex(zIndex: Float)
```

创建一个修饰符，用于控制同一布局父项的子项的绘制顺序。
</td></tr>

</table>

## 焦点

<table>


<tr><td>

作用域：任意 

</td>
<td>
``` kotlin
Modifier.focusModifier()
```

此函数已弃用。取而代之的是 focusTarget
</td></tr>

<tr><td>

作用域：任意 

</td>
<td>
``` kotlin
Modifier.focusOrder(focusOrderReceiver: FocusOrder.() -> Unit)
```

使用此修饰符可指定自定义焦点遍历顺序。
</td></tr>

<tr><td>

作用域：任意 

</td>
<td>
``` kotlin
Modifier.focusOrder(focusRequester: FocusRequester)
```

此修饰符可让您为当前可组合项指定一个 FocusRequester，以便其他可组合项可以使用此 focusRequester 指定自定义焦点顺序。
</td></tr>

<tr><td>

作用域：任意 

</td>
<td>
``` kotlin
Modifier.focusOrder(
    focusRequester: FocusRequester,
    focusOrderReceiver: FocusOrder.() -> Unit
)
```

此修饰符可让您为当前可组合项指定一个 FocusRequester 以及 focusOrder。
</td></tr>

<tr><td>

作用域：任意 

</td>
<td>
``` kotlin
Modifier.focusRequester(focusRequester: FocusRequester)
```

将此修饰符添加到组件，以观察焦点状态的变化。
</td></tr>

<tr><td>

作用域：任意 

</td>
<td>
``` kotlin
Modifier.focusTarget()
```

将此修饰符添加到组件，以使其可聚焦。
</td></tr>

<tr><td>

作用域：任意 

</td>
<td>
``` kotlin
Modifier.focusable(
    enabled: Boolean,
    interactionSource: MutableInteractionSource?
)
```

将组件配置为可通过焦点系统或无障碍“焦点”事件聚焦。
</td></tr>

<tr><td>

作用域：任意 

</td>
<td>
``` kotlin
Modifier.onFocusChanged(onFocusChanged: (FocusState) -> Unit)
```

将此修饰符添加到组件，以观察焦点状态事件。
</td></tr>

<tr><td>

作用域：任意 

</td>
<td>
``` kotlin
Modifier.onFocusEvent(onFocusEvent: (FocusState) -> Unit)
```

将此修饰符添加到组件，以观察焦点状态事件。
</td></tr>
</table>

## 图形

<table>


<tr><td>

作用域：任意 

</td>
<td>
``` kotlin
Modifier.graphicsLayer(
    scaleX: Float,
    scaleY: Float,
    alpha: Float,
    translationX: Float,
    translationY: Float,
    shadowElevation: Float,
    rotationX: Float,
    rotationY: Float,
    rotationZ: Float,
    cameraDistance: Float,
    transformOrigin: TransformOrigin,
    shape: Shape,
    clip: Boolean
)
```

此 Modifier.Element 可使内容绘制到绘图层。
</td></tr>

<tr><td>

作用域：任意 

</td>
<td>
``` kotlin
Modifier.graphicsLayer(block: GraphicsLayerScope.() -> Unit)
```

此 Modifier.Element 可使内容绘制到绘图层。
</td></tr>

<tr><td>

作用域：任意 

</td>
<td>
``` kotlin
Modifier.toolingGraphicsLayer()
```

此 Modifier.Element 可添加绘制图层，以便工具可以识别所绘制图片中的元素。
</td></tr>

</table>

## 键盘

<table>


<tr><td>

作用域：任意 

</td>
<td>
``` kotlin
Modifier.onKeyEvent(onKeyEvent: (KeyEvent) -> Boolean)
```

将此 modifier 添加到组件的 modifier 参数中后，它可以拦截硬件按键事件。
</td></tr>

<tr><td>

作用域：任意 

</td>
<td>
``` kotlin
Modifier.onPreviewKeyEvent(onPreviewKeyEvent: (KeyEvent) -> Boolean)
```

将此 modifier 添加到组件的 modifier 参数中后，它可以拦截硬件按键事件。
</td></tr>

</table>

## 布局

<table>


<tr><td>

作用域：任意 

</td>
<td>
``` kotlin
Modifier.layout(
    measure: MeasureScope.(Measurable, Constraints) -> MeasureResult
)
```

创建 LayoutModifier，以允许更改已封装元素的测量和布局方式。
</td></tr>

<tr><td>

作用域：任意 

</td>
<td>
``` kotlin
Modifier.layoutId(layoutId: Any)
```

使用 layoutId 标记元素，以在其父项中识别它。
</td></tr>

<tr><td>

作用域：任意 

</td>
<td>
``` kotlin
Modifier.onGloballyPositioned(
    onGloballyPositioned: (LayoutCoordinates) -> Unit
)
```

当内容的全局位置可能发生变化时，使用元素的 LayoutCoordinates 调用 onGloballyPositioned。
</td></tr>

</table>

## 内边距

<table>


<tr><td>

作用域：任意 

</td>
<td>
``` kotlin
Modifier.absolutePadding(left: Dp, top: Dp, right: Dp, bottom: Dp)
```

沿着内容的每个边缘应用 Dp 的额外空间：left、top、right 和 bottom。
</td></tr>

<tr><td>

作用域：任意 

</td>
<td>
``` kotlin
Modifier.padding(start: Dp, top: Dp, end: Dp, bottom: Dp)
```

沿着内容的每个边缘应用 Dp 的额外空间：start、top、end 和 bottom。
</td></tr>

<tr><td>

作用域：任意 

</td>
<td>
``` kotlin
Modifier.padding(horizontal: Dp, vertical: Dp)
```

沿着内容的左侧和右侧边缘应用 horizontal dp 空间，并沿着顶部和底部边缘应用 vertical dp 空间。
</td></tr>

<tr><td>

作用域：任意 

</td>
<td>
``` kotlin
Modifier.padding(all: Dp)
```

沿着内容的每个边缘（左侧、顶部、右侧和底部）应用 all dp 的额外空间。
</td></tr>

<tr><td>

作用域：任意 

</td>
<td>
``` kotlin
Modifier.padding(paddingValues: PaddingValues)
```

向组件应用 PaddingValues，将其作为沿内容左侧、顶部、右侧和底部的额外空间。
</td></tr>

<tr><td>

作用域：任意 

</td>
<td>
``` kotlin
Modifier.paddingFrom(alignmentLine: AlignmentLine, before: Dp, after: Dp)
```

此 Modifier 可添加内边距，以根据从内容边界到 alignment line 的指定距离放置内容。
</td></tr>

<tr><td>

作用域：任意 

</td>
<td>
``` kotlin
Modifier.paddingFrom(
    alignmentLine: AlignmentLine,
    before: TextUnit,
    after: TextUnit
)
```

此 Modifier 可添加内边距，以根据从内容边界到 alignment line 的指定距离放置内容。
</td></tr>

<tr><td>

作用域：任意 

</td>
<td>
``` kotlin
Modifier.paddingFromBaseline(top: Dp, bottom: Dp)
```

此 Modifier 用于在布局中放置内容，使从布局顶部到 baseline of the first line of text in the content 的距离为 top，且从 baseline of the last line of text in the content 到布局底部的距离为 bottom。
</td></tr>

<tr><td>

作用域：任意 

</td>
<td>
``` kotlin
Modifier.paddingFromBaseline(top: TextUnit, bottom: TextUnit)
```

此 Modifier 用于在布局中放置内容，使从布局顶部到 baseline of the first line of text in the content 的距离为 top，且从 baseline of the last line of text in the content 到布局底部的距离为 bottom。
</td></tr>

</table>

## 指针

<table>


<tr><td>

作用域：任意 

</td>
<td>
``` kotlin
Modifier.pointerInput(block: suspend PointerInputScope.() -> Unit)
```

此函数已弃用。Modifier.pointerInput 必须提供一个或多个“key”参数来定义此修饰符的身份，以及确定应在何时取消它的上一个输入处理协程并为新键启动新效果。
</td></tr>

<tr><td>

作用域：任意 

</td>
<td>
``` kotlin
Modifier.pointerInput(
    key1: Any?,
    block: suspend PointerInputScope.() -> Unit
)
```

创建一个用于在修饰的元素区域内处理指针输入的修饰符。
</td></tr>

<tr><td>

作用域：任意 

</td>
<td>
``` kotlin
Modifier.pointerInput(
    key1: Any?,
    key2: Any?,
    block: suspend PointerInputScope.() -> Unit
)
```

创建一个用于在修饰的元素区域内处理指针输入的修饰符。
</td></tr>

<tr><td>

作用域：任意 

</td>
<td>
``` kotlin
Modifier.pointerInput(
    keys: vararg Any?,
    block: suspend PointerInputScope.() -> Unit
)

```

创建一个用于在修饰的元素区域内处理指针输入的修饰符。
</td></tr>

<tr><td>

作用域：任意 

@ExperimentalComposeUiApi 
</td>
<td>
``` kotlin
Modifier.pointerInteropFilter(
    requestDisallowInterceptTouchEvent: RequestDisallowInterceptTouchEvent?,
    onTouchEvent: (MotionEvent) -> Boolean
)
```

一个特殊的 PointerInputModifier，可提供对最初分派到 Compose 的底层 MotionEvent 的访问权限。
</td></tr>

</table>

## 位置

<table>


<tr><td>

作用域：任意 

</td>
<td>
``` kotlin
Modifier.absoluteOffset(x: Dp, y: Dp)
```

将内容偏移（x dp，y dp）。
</td></tr>

<tr><td>

作用域：任意 

</td>
<td>
``` kotlin
Modifier.absoluteOffset(offset: Density.() -> IntOffset)
```

将内容偏移 offset 像素。
</td></tr>

<tr><td>

作用域：任意 

</td>
<td>
``` kotlin
Modifier.offset(x: Dp, y: Dp)
```

将内容偏移（x dp，y dp）。
</td></tr>

<tr><td>

作用域：任意 

</td>
<td>
``` kotlin
Modifier.offset(offset: Density.() -> IntOffset)
```

将内容偏移 offset 像素。
</td></tr>

<tr><td>

作用域：TabRowDefaults

</td>
<td>
``` kotlin
Modifier.tabIndicatorOffset(currentTabPosition: TabPosition)
```

此 Modifier 会占用 TabRow 内的所有可用宽度，然后以动画方式呈现它应用到指示器的偏移量（具体取决于 currentTabPosition）。
</td></tr>

</table>

## 语义

<table>


<tr><td>

作用域：任意 

</td>
<td>
``` kotlin
Modifier.clearAndSetSemantics(
    properties: SemanticsPropertyReceiver.() -> Unit
)
```

清除所有后代节点的语义并设置新语义。
</td></tr>

<tr><td>

作用域：任意 

</td>
<td>
``` kotlin
Modifier.progressSemantics(
    value: Float,
    valueRange: ClosedFloatingPointRange<Float>,
    steps: Int
)
```

包含确定性进度指示器或滑块的进度部分所需的 semantics，它表示 valueRange 内的进度。
</td></tr>

<tr><td>

作用域：任意 

</td>
<td>
``` kotlin
Modifier.progressSemantics()
```

包含不确定性进度指示器所需的 semantics，它表示操作正在进行中。
</td></tr>

<tr><td>

作用域：任意 

</td>
<td>
``` kotlin
Modifier.semantics(
    mergeDescendants: Boolean,
    properties: SemanticsPropertyReceiver.() -> Unit
)
```

将语义键值对添加到布局节点，以便用于测试、无障碍功能等。
</td></tr>

</table>

## 滚动

<table>


<tr><td>

作用域：任意 

</td>
<td>
``` kotlin
Modifier.horizontalScroll(
    state: ScrollState,
    enabled: Boolean,
    flingBehavior: FlingBehavior?,
    reverseScrolling: Boolean
)
```

修改元素，以支持在内容的宽度大于允许的最大约束时水平滚动。
</td></tr>

<tr><td>

作用域：任意 

</td>
<td>
``` kotlin
Modifier.nestedScroll(
    connection: NestedScrollConnection,
    dispatcher: NestedScrollDispatcher?
)
```

修改元素，以使其参与嵌套滚动层次结构。
</td></tr>

<tr><td>

作用域：任意 

@ExperimentalComposeUiApi 
</td>
<td>
``` kotlin
Modifier.relocationRequester(
    relocationRequester: RelocationRequester
    )

```

此修饰符可用于发送重定位请求。
</td></tr>

<tr><td>

作用域：任意 

</td>
<td>
``` kotlin
	
Modifier.scrollable(
    state: ScrollableState,
    orientation: Orientation,
    enabled: Boolean,
    reverseDirection: Boolean,
    flingBehavior: FlingBehavior?,
    interactionSource: MutableInteractionSource?
)
```

在单个 Orientation 为界面元素配置触摸滚动和快速滑动。
</td></tr>

<tr><td>

作用域：任意 

</td>
<td>
``` kotlin
Modifier.verticalScroll(
    state: ScrollState,
    enabled: Boolean,
    flingBehavior: FlingBehavior?,
    reverseScrolling: Boolean
)
```

修改元素，以支持在内容的高度大于允许的最大约束时垂直滚动。
</td></tr>

</table>

## 尺寸

<table>


<tr><td>

作用域：任意 

</td>
<td>
``` kotlin
Modifier.animateContentSize(
    animationSpec: FiniteAnimationSpec<IntSize>,
    finishedListener: (IntSize, IntSize) -> Unit
)
```

此修饰符会在其子修饰符（或子可组合项，如果它已位于链尾）更改尺寸时为其自己的尺寸添加动画效果。
</td></tr>

<tr><td>

作用域：任意 

</td>
<td>
``` kotlin
Modifier.aspectRatio(ratio: Float, matchHeightConstraintsFirst: Boolean)
```

通过尝试按照以下顺序匹配传入的某个约束条件来尝试调整内容尺寸，使其符合指定的宽高比：如果 matchHeightConstraintsFirst 为 false（默认值），则顺序为 Constraints.maxWidth、Constraints.maxHeight、Constraints.minWidth、Constraints.minHeight；如果 matchHeightConstraintsFirst 为 true，则顺序为 Constraints.maxHeight、Constraints.maxWidth、Constraints.minHeight、Constraints.minWidth。
</td></tr>

<tr><td>

作用域：任意 

</td>
<td>
``` kotlin
Modifier.defaultMinSize(minWidth: Dp, minHeight: Dp)
```

仅在封装的布局不受约束时约束其尺寸：minWidth 和 minHeight 约束仅在传入的相应约束为 0 时应用。
</td></tr>

<tr><td>

作用域：任意 

</td>
<td>
``` kotlin
Modifier.fillMaxHeight(fraction: Float)
```

让内容填充（可能仅部分填充）传入的测量约束的 Constraints.maxHeight，方法是将 minimum height 和 maximum height 设置为等于 fraction 乘以 maximum height 得出的值。
</td></tr>

<tr><td>

作用域：任意 

</td>
<td>
``` kotlin
Modifier.fillMaxSize(fraction: Float)
```

让内容填充（可能仅部分填充）传入的测量约束的 Constraints.maxWidth 和 Constraints.maxHeight，方法是将 minimum width 和 maximum width 设置为等于 fraction 乘以 maximum width 得出的值，并将 minimum height 和 maximum height 设置为等于 fraction 乘以 maximum height 得出的值。
</td></tr>

<tr><td>

作用域：任意 

</td>
<td>
``` kotlin
Modifier.fillMaxWidth(fraction: Float)
```

让内容填充（可能仅部分填充）传入的测量约束的 Constraints.maxWidth，方法是将 minimum width 和 maximum width 设置为等于 fraction 乘以 maximum width 得出的值。
</td></tr>

<tr><td>

作用域：任意 

</td>
<td>
``` kotlin
Modifier.height(intrinsicSize: IntrinsicSize)
```

将内容的首选高度声明为与内容的最小或最大固有高度相同。
</td></tr>

<tr><td>

作用域：任意 

</td>
<td>
``` kotlin
Modifier.height(height: Dp)
```

将内容的首选高度声明为正好 heightdp。
</td></tr>

<tr><td>

作用域：任意 

</td>
<td>
``` kotlin
Modifier.heightIn(min: Dp, max: Dp)
```

如果传入的测量 Constraints 允许，将内容的高度限制在 mindp 与 maxdp 之间。
</td></tr>

<tr><td>

作用域：任意 

</td>
<td>
``` kotlin
Modifier.onSizeChanged(onSizeChanged: (IntSize) -> Unit)
```

当紧跟在 onSizeChanged 后面的修饰符的尺寸发生变化时，调用它。
</td></tr>

<tr><td>

作用域：任意 

</td>
<td>
``` kotlin
Modifier.requiredHeight(intrinsicSize: IntrinsicSize)
```

将内容的高度声明为与内容的最小或最大固有高度完全相同。
</td></tr>

<tr><td>

作用域：任意 

</td>
<td>
``` kotlin
Modifier.requiredHeight(height: Dp)
```

将内容的高度声明为正好 height dp。
</td></tr>

<tr><td>

作用域：任意 

</td>
<td>
``` kotlin
Modifier.requiredHeightIn(min: Dp, max: Dp)
```

将内容的高度限制在 mindp 与 maxdp 之间。
</td></tr>

<tr><td>

作用域：任意 

</td>
<td>
``` kotlin
Modifier.requiredSize(size: Dp)
```

将内容的尺寸声明为宽度和高度正好是 sizedp。
</td></tr>

<tr><td>

作用域：任意 

</td>
<td>
``` kotlin
Modifier.requiredSize(width: Dp, height: Dp)
```

将内容的尺寸声明为正好是 widthdp 和 heightdp。
</td></tr>


<tr><td>

作用域：任意 

</td>
<td>
``` kotlin
Modifier.requiredSizeIn(
    minWidth: Dp,
    minHeight: Dp,
    maxWidth: Dp,
    maxHeight: Dp
)
```

将内容的宽度限制在 minWidthdp 与 maxWidthdp 之间，并将内容的高度限制在 minHeightdp 与 maxHeightdp 之间。
</td></tr>

<tr><td>

作用域：任意 

</td>
<td>
``` kotlin
Modifier.requiredWidth(intrinsicSize: IntrinsicSize)
```

将内容的宽度声明为与内容的最小或最大固有宽度完全相同。
</td></tr>

<tr><td>

作用域：任意 

</td>
<td>
``` kotlin
Modifier.requiredWidth(width: Dp)
```

将内容的宽度声明为正好 widthdp。
</td></tr>

<tr><td>

作用域：任意 

</td>
<td>
``` kotlin
Modifier.requiredWidthIn(min: Dp, max: Dp)
```

将内容的宽度限制在 mindp 与 maxdp 之间。
</td></tr>

<tr><td>

作用域：任意 

</td>
<td>
``` kotlin
Modifier.size(size: Dp)
```

将内容的首选尺寸声明为正好是 sizedp 的方形。
</td></tr>


<tr><td>

作用域：任意 

</td>
<td>
``` kotlin
Modifier.size(width: Dp, height: Dp)
```

将内容的首选尺寸声明为正好是 widthdp x heightdp。
</td></tr>

<tr><td>

作用域：任意 

</td>
<td>
``` kotlin
Modifier.sizeIn(minWidth: Dp, minHeight: Dp, maxWidth: Dp, maxHeight: Dp)
```

如果传入的测量 Constraints 允许，将内容的宽度限制在 minWidthdp 与 maxWidthdp 之间，并将内容的高度限制在 minHeightdp 与 maxHeightdp 之间。
</td></tr>

<tr><td>

作用域：任意 

</td>
<td>
``` kotlin
Modifier.width(intrinsicSize: IntrinsicSize)
```

将内容的首选宽度声明为与内容的最小或最大固有宽度相同。
</td></tr>

<tr><td>

作用域：任意 

</td>
<td>
``` kotlin
Modifier.width(width: Dp)
```

将内容的首选宽度声明为正好 widthdp。
</td></tr>

<tr><td>

作用域：任意 

</td>
<td>
``` kotlin
Modifier.widthIn(min: Dp, max: Dp)
```

如果传入的测量 Constraints 允许，将内容的宽度限制在 mindp 与 maxdp 之间。
</td></tr>


<tr><td>

作用域：任意 

</td>
<td>
``` kotlin
Modifier.wrapContentHeight(
    align: Alignment.Vertical,
    unbounded: Boolean
)
```

允许在不考虑传入的测量 minimum height constraint 的情况下将内容测量为所需高度；如果 unbounded 为 true，则也可不考虑传入的测量 maximum height constraint。
</td></tr>

<tr><td>

作用域：任意 

</td>
<td>
``` kotlin
Modifier.wrapContentSize(align: Alignment, unbounded: Boolean)
```

允许在不考虑传入的测量 minimum width 或 minimum height 约束的情况下将内容测量为所需大小；如果 unbounded 为 true，则也可不考虑传入的最大约束。
</td></tr>

<tr><td>

作用域：任意 

</td>
<td>
``` kotlin
Modifier.wrapContentWidth(
    align: Alignment.Horizontal,
    unbounded: Boolean
)
```

允许在不考虑传入的测量 minimum width constraint 的情况下将内容测量为所需宽度；如果 unbounded 为 true，则也可不考虑传入的测量 maximum width constraint。
</td></tr>

<tr><td>

作用域：LazyItemScope

</td>
<td>
``` kotlin
Modifier.fillParentMaxHeight(fraction: Float)
```

让内容填充传入的测量约束的 Constraints.maxHeight，方法是将 minimum height 设置为等于 maximum height 乘以 fraction 得出的值。
</td></tr>

<tr><td>

作用域：LazyItemScope 

</td>
<td>
``` kotlin
Modifier.fillParentMaxSize(fraction: Float)
```

让内容填充父级测量约束的 Constraints.maxWidth 和 Constraints.maxHeight，方法是将 minimum width 设置为等于 maximum width 乘以 fraction 得出的值，并将 minimum height 设置为等于 maximum height 乘以 fraction 得出的值。
</td></tr>

<tr><td>

作用域：LazyItemScope 

</td>
<td>
``` kotlin
Modifier.fillParentMaxWidth(fraction: Float)
```

让内容填充父级测量约束的 Constraints.maxWidth，方法是将 minimum width 设置为等于 maximum width 乘以 fraction 得出的值。
</td></tr>

<tr><td>

作用域：BoxScope

</td>
<td>
``` kotlin
Modifier.matchParentSize()
```

在测量完所有其他内容元素后，调整元素的尺寸，使其与 Box 的尺寸一致。
</td></tr>

<tr><td>

作用域：RowScope

</td>
<td>
``` kotlin
Modifier.weight(weight: Float, fill: Boolean)
```

根据元素相对于 Row 中其他加权同级元素的 weight，按比例调整元素的宽度。
</td></tr>

<tr><td>

作用域：ColumnScope

</td>
<td>
``` kotlin
Modifier.weight(weight: Float, fill: Boolean)
```

根据元素相对于 Column 中其他加权同级元素的 weight，按比例调整元素的高度。
</td></tr>

</table>

## 测试

<table>


<tr><td>

作用域：任意 

</td>
<td>
``` kotlin
Modifier.testTag(tag: String)
```

应用标记以允许在测试中找到修饰的元素。
</td></tr>

</table>

## 变换

<table>


<tr><td>

作用域：任意 

</td>
<td>
``` kotlin
Modifier.rotate(degrees: Float)
```

设置视图围绕可组合项中心旋转的角度。
</td></tr>

<tr><td>

作用域：任意 

</td>
<td>
``` kotlin
Modifier.scale(scaleX: Float, scaleY: Float)
```

分别按以下缩放比例沿水平轴和垂直轴缩放可组合项的内容。
</td></tr>

<tr><td>

作用域：任意 

</td>
<td>
``` kotlin
Modifier.scale(scale: Float)

```

按相同的缩放比例沿水平轴和垂直轴均匀缩放内容。
</td></tr>

<tr><td>

作用域：任意 

</td>
<td>
``` kotlin
Modifier.transformable(
    state: TransformableState,
    lockRotationOnZoomPan: Boolean,
    enabled: Boolean
)
```

启用修饰的界面元素的变换手势。
</td></tr>

</table>

<table>

<tr><td>

作用域：任意 

</td>
<td>
``` kotlin
Modifier.composed(
    inspectorInfo: InspectorInfo.() -> Unit,
    factory: Modifier.() -> Modifier
)
```

声明将针对所修饰的每个元素进行组合的 Modifier 的即时组合。
</td></tr>

<tr><td>

作用域：任意 

</td>
<td>
``` kotlin
Composer.materialize(modifier: Modifier)
```

将任何特定于实例的 composed modifiers 具体化，以应用到原始树节点。
</td></tr>

</table>