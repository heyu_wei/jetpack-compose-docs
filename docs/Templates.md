## File Templates

有两种方式能找到编辑File Templates的入口

- 在 File -> New -> Edit File Templates 或者 在项目结构目录中点击右键菜单 New -> Edit File Templates 
  ![0](assets/template1.png)
- 在设置界面中的 Editor -> File and Code Templates
  ![0](assets/template2.png)


```
#if (${PACKAGE_NAME} && ${PACKAGE_NAME} != "")package ${PACKAGE_NAME}

#end
#parse("File Header.java")

import androidx.compose.runtime.Composable
import androidx.compose.ui.tooling.preview.Preview

#if (${Function_Name} == "" )
@Composable
fun ${NAME}() {
}

@Preview
@Composable
fun ${NAME}Preview() {
    ${NAME}()
}
#end

#if (${Function_Name} != "" )
@Composable
fun ${Function_Name}() {
}

@Preview
@Composable
fun ${Function_Name}Preview() {
    ${Function_Name}()
}
#end
```

复制上面的代码，按照步骤填写

![3](assets/template3.png)

完成上面步骤后，就可以使用创建好的模版了

![4](assets/template4.png)

![4](assets/template5.png)

## Live Templates

在设置界面中的 Editor -> Live Templates 找到模版列表

![6](assets/template6.png)

可以看到里面有很多分组，我们可以在 AndroidCompose 分组下增加新的模版，点击选中 AndroidCompose 分组，然后在右侧点击 + 号，选择新增 Live Templates


```
@androidx.compose.runtime.Composable
fun $NAME$() {
$END$
}

@androidx.compose.ui.tooling.preview.Preview
@androidx.compose.runtime.Composable
fun $NAME$Preview() {
$END$
}
```

复制上面的代码，按照步骤填写

![6](assets/template7.png)

选择模版生效范围，在这里可以只选择 Top-Level，可以全部选择 kotlin 下

![6](assets/template8.png)

然后点击 OK 就可以保存模版了，接下来就是使用这个模版，随便新建一个 kotlin 空白文件中，输入 comp，即出现下面提示

[![6](assets/template9.png)](assets/template9.png)

回车后，即可按照模版新建出一段代码，输入方法名即可。

## 视频教程

<a href="https://www.bilibili.com/video/BV1WR4y1L7Yn">https://www.bilibili.com/video/BV1WR4y1L7Yn</a>

<a href="https://www.bilibili.com/video/BV1aS4y1D7dv/">实战视频</a>