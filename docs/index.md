## Jetpack Compose 是什么？

!!! cite
    Jetpack Compose 是用于构建原生 Android 界面的新工具包。它可简化并加快 Android 上的界面开发，使用更少的代码、强大的工具和直观的 Kotlin API，快速让应用生动而精彩。

!!! Example
    <center>
    <img src="assets/landing-code-static.svg" width="70%" align=left/><img src="assets/landing-preview-animation.gif" width="30%" align=right/>
    </center>


## 一些建议

* 先学一下 <a href="https://kotlinlang.org/">Kotlin</a> 语言
* 最好有 Android App开发经验，至少应该有移动 APP 开发经验

## 视频教程

- <a href="https://www.bilibili.com/video/BV1oq4y147zJ/?spm_id_from=333.788">基础姿势</a>
- <a href="https://www.bilibili.com/video/BV1aS4y1D7dv/">实战视频</a>