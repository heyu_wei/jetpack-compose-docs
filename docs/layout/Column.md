## 属性

``` kotlin
@Composable
inline fun Column(
    modifier: Modifier? = Modifier,
    verticalArrangement: Arrangement.Vertical? = Arrangement.Top,
    horizontalAlignment: Alignment.Horizontal? = Alignment.Start,
    content: (@Composable @ExtensionFunctionType ColumnScope.() -> Unit)?
): Unit
```

Column 布局可以让其子组件呈纵向排列，在 ColumnScope 中可以使用 Modifier.weight 来设置子组件所占比重。

## 参数

- verticalArrangement

  当 Column 的大小比它的子组件大小之和还要大的话，那可以通过verticalArrangement来设置不同的排列方式

  ![column_arrangement_visualization](../assets/column_arrangement_visualization.gif)

- horizontalAlignment 设置子组件的对齐方式，包括Alignment.Start、Alignment.End、Alignment.CenterHorizontal

``` kotlin
@Composable
fun ColumnSample() {
    Column(
        modifier = Modifier
            .requiredHeight(100.dp)
            .background(Color.White),
    ) {
        Text(
            text = "First ", modifier = Modifier
                .background(Color.Red)
                .weight(0.7f,fill=false)
        )

        Text("Second", modifier = Modifier
            .background(Color.Green)
            .weight(0.3f))
    }
}
```

下图可以看到在设置 weight 的时候```fill=true``` 和 ```fill=false``` 的区别，默认情况下 ```fill = true```

![column](../assets/column.png)

## 视频教程

<a href="https://www.bilibili.com/video/BV1TP4y1A7u5" target="_blank">https://www.bilibili.com/video/BV1TP4y1A7u5</a>

<a href="https://www.bilibili.com/video/BV1aS4y1D7dv/">实战视频</a>