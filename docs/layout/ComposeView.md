## 想在传统 xml 布局项目中使用 Compose 怎么办？

Compose 工具包已经提供了 ```androidx.compose.ui.platform.ComposeView``` 来在 xml 中增加 Compose UI。

只需要在对应的 xml 中使用即可

```xml
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:orientation="vertical">

    <TextView
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:text="Text View From XML" />

    <androidx.compose.ui.platform.ComposeView
        android:id="@+id/composeView"
        android:layout_width="match_parent"
        android:layout_height="wrap_content" />

</LinearLayout>
```

在 Activity 中通过 findViewById 或者 view-binding 拿到 composeView 组件后，就可以调用 setContent 方法，在方法体中就可以编写 Compose 代码

``` kotlin
val composeView = findViewById<ComposeView>(R.id.composeView)
composeView.setContent {
  Column {
    Text(text = "Text From Compose")
    Text(text = "Text From Compose")
    Text(text = "Text From Compose")
  }
}
```

## 想在 Compose 中使用 View 怎么办？

同样系统也提供了 AndroidView 来进行这样的操作。在上面的例子中，我们可以这样写

``` kotlin
val composeView = findViewById<ComposeView>(R.id.composeView)

composeView.setContent {

    Column {
        Text(text = "Text From Compose")
        Text(text = "Text From Compose")
        Text(text = "Text From Compose")

        AndroidView(factory = {
            TextView(it)
        }) {
            it.apply {
                text = "这是从 Compose 里旋转的 xml 布局的 TextView"
            }
        }
    }

}
```

怎么样！是不是可以套娃了

## 视频教程

- <a href="https://www.bilibili.com/video/BV1CL411N7BL/">https://www.bilibili.com/video/BV1CL411N7BL/</a>
- <a href="https://www.bilibili.com/video/BV1aS4y1D7dv/">实战视频</a>