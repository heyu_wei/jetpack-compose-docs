## 属性

``` kotlin
@Composable
@ExperimentalMaterialApi
fun ListItem(
    modifier: Modifier? = Modifier,
    icon: (@Composable () -> Unit)? = null,
    secondaryText: (@Composable () -> Unit)? = null,
    singleLineSecondaryText: Boolean? = true,
    overlineText: (@Composable () -> Unit)? = null,
    trailing: (@Composable () -> Unit)? = null,
    text: (@Composable () -> Unit)?
): Unit
```

系统预设列表项组件

## 参数

- icon 设置左侧图标
- text 主文本
- secondaryText 副文本
- singleLineSecondaryText 副文本是否单行显示
- overlineText 显示在主文本上面的文本
- trailing 设置右侧内容，通常是Icon、CheckBox 或 Switch

``` kotlin
@OptIn(ExperimentalMaterialApi::class)
@Composable
fun ListItemSample() {
    Column {
        ListItem(text = { Text("text") }, icon = {
            Icon(
                imageVector = Icons.Default.BrokenImage,
                contentDescription = null,
                modifier = Modifier.size(50.dp)
            )
        }, secondaryText = {
            Text("secondaryText")
        }, overlineText = {
            Text("overlineText")
        }, trailing = {
            Text("trailing")
        })
    }
}
```

![ListItem](../assets/ListItem.png)

## 视频教程

<a href="https://www.bilibili.com/video/BV1cY411V7M4">https://www.bilibili.com/video/BV1cY411V7M4</a>

<a href="https://www.bilibili.com/video/BV1aS4y1D7dv/">实战视频</a>