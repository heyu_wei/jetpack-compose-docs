## 属性

``` kotlin
@Composable
@NonRestartableComposable
fun Card(
    modifier: Modifier? = Modifier,
    shape: Shape? = MaterialTheme.shapes.medium,
    backgroundColor: Color? = MaterialTheme.colors.surface,
    contentColor: Color? = contentColorFor(backgroundColor),
    border: BorderStroke? = null,
    elevation: Dp? = 1.dp,
    content: (@Composable () -> Unit)?
): Unit
```

## 参数

- backgroudColor
- contentColor
- border
- elevation 抬高

``` kotlin
@Composable
fun CardSample() {
    Card(
        modifier = Modifier.padding(8.dp),
        backgroundColor = Color.Green,
        contentColor = Color.White,
        border = BorderStroke(1.dp, Color.Red),
        elevation = 10.dp
    ) {
        Text("Cart Content", Modifier.padding(8.dp), color = Color.Red)
    }
}
```

![card](../assets/card.png)

## 视频教程

<a href="https://www.bilibili.com/video/BV1Gi4y127JL" target="_blank">https://www.bilibili.com/video/BV1Gi4y127JL</a>

<a href="https://www.bilibili.com/video/BV1aS4y1D7dv/">实战视频</a>