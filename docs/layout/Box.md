## 属性

``` kotlin
@Composable
fun Box(modifier: Modifier?): Unit
```

Box 布局中所有子组件会从左上角开始堆叠在一起。

``` kotlin
@Composable
fun BoxSample() {
    Box {
        Box(Modifier.fillMaxSize().background(Color.Cyan))
        Box(
            Modifier.matchParentSize()
                .padding(top = 20.dp, bottom = 20.dp)
                .background(Color.Yellow)
        )
        Box(
            Modifier.matchParentSize()
                .padding(40.dp)
                .background(Color.Magenta)
        )
        Box(
            Modifier.align(Alignment.Center)
                .size(300.dp, 300.dp)
                .background(Color.Green)
        )
        Box(
            Modifier.align(Alignment.TopStart)
                .size(150.dp, 150.dp)
                .background(Color.Red)
        )
        Box(
            Modifier.align(Alignment.BottomEnd)
                .size(150.dp, 150.dp)
                .background(Color.Blue)
        )
    }
}
```

![box](../assets/box.png)

## BoxWithConstraints

与 Box 布局一样，只是可以在 BoxWithConstraintsScope 中获取到相关信息

- constraints 父控件的约束信息，包含 minWidth、maxWidth、miniHeight、maxHeight 等像素单位数据
- minWidth 最小宽度的 dp 值
- maxWidth 最大宽度的 dp 值
- miniHeight 最小高度的 dp 值
- maxHeight 最大高度的 dp 值

例如，可以根据大小来判断显示不同的界面

``` kotlin
@Composable
fun BoxWithConstraintsSample() {
    BoxWithConstraints(modifier = Modifier.size(100.dp)) {
        val rectangleHeight = 100.dp
        if (maxHeight < rectangleHeight * 2) {
            Box(Modifier.size(50.dp, rectangleHeight).background(Color.Blue))
        } else {
            Box(Modifier.size(50.dp, rectangleHeight).background(Color.Red))
        }
    }
}
```

## 视频教程

<a href="https://www.bilibili.com/video/BV1pi4y127HC" target="_blank">https://www.bilibili.com/video/BV1pi4y127HC</a>

<a href="https://www.bilibili.com/video/BV1aS4y1D7dv/">实战视频</a>