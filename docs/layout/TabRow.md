## 属性

``` kotlin
@Composable
fun TabRow(
    selectedTabIndex: Int?,
    modifier: Modifier? = Modifier,
    backgroundColor: Color? = MaterialTheme.colors.primarySurface,
    contentColor: Color? = contentColorFor(backgroundColor),
    indicator: (@Composable (tabPositions: List<TabPosition>) -> Unit)? = @Composable { tabPositions ->
        TabRowDefaults.Indicator(
            Modifier.tabIndicatorOffset(tabPositions[selectedTabIndex])
        )
    },
    divider: (@Composable () -> Unit)? = @Composable {
        TabRowDefaults.Divider()
    },
    tabs: (@Composable () -> Unit)?
): Unit
```

- selectedTabIndex 当前选中的 Tab 下标
- indicator 设置指示器
- divider 设置底部分割线
- tabs 设置Tab 数组，通常可以设置为 Tab 或 LeadingIconTab

``` kotlin
@OptIn(ExperimentalMaterialApi::class)
@Composable
fun TabRowSample() {
    var selectedIndex by remember {
        mutableStateOf(0)
    }
    Column() {
        TabRow(
            selectedTabIndex = selectedIndex,
            contentColor = Color.Yellow,
            indicator = {}
        ) {
            LeadingIconTab(
                selected = selectedIndex == 0,
                text = {
                    Text(text = "Tab0")
                },
                icon = {
                    Icon(
                        imageVector = Icons.Default.AccountBox,
                        contentDescription = null
                    )
                },
                selectedContentColor = Color.Red,
                unselectedContentColor = Color.White,
                onClick = { selectedIndex = 0 })
            Tab(selected = selectedIndex == 1, text = {
                Text(text = "Tab1")
            }, onClick = { selectedIndex = 1 })
            Tab(selected = selectedIndex == 2, text = {
                Text(text = "Tab2")
            }, onClick = { selectedIndex = 2 })
        }
        Text("current index : $selectedIndex")
    }
}
```

![TabRow1](../assets/TabRow1.png)

## ScrollableTabRow

如果想让 TabRow 内容较多时可以滚动的话，请使用ScrollableTabRow

## 视频教程

- <a href="https://www.bilibili.com/video/BV1eY411G74F/">https://www.bilibili.com/video/BV1eY411G74F/</a>
- <a href="https://www.bilibili.com/video/BV1aS4y1D7dv/">实战视频</a>