## 属性

``` kotlin
@Composable
fun LazyRow(
    modifier: Modifier? = Modifier,
    state: LazyListState? = rememberLazyListState(),
    contentPadding: PaddingValues? = PaddingValues(0.dp),
    reverseLayout: Boolean? = false,
    horizontalArrangement: Arrangement.Horizontal? = if (!reverseLayout) Arrangement.Start else Arrangement.End,
    verticalAlignment: Alignment.Vertical? = Alignment.Top,
    flingBehavior: FlingBehavior? = ScrollableDefaults.flingBehavior(),
    userScrollEnabled: Boolean? = true,
    content: (@ExtensionFunctionType LazyListScope.() -> Unit)?
): Unit
```

可滚动的且自动回收的 Row

## 参数

- state 管理列表的状态，可通过这个操作列表滚动

在 LazyListScpoe 中，可以使用以下语句进行操作：

- item 添加单个列表项
- items 添加一组列表项
- itemsIndexed 
- stickyHeader 添加粘性头部

``` kotlin
@OptIn(ExperimentalFoundationApi::class)
@Composable
fun LazyRowSample() {
    val items = listOf(1, 2, 3, 4, 5, 6, 7, 8, 9, 10)

    LazyRow() {
        stickyHeader { Text(text = "11111") }
        items(items) {
            Text(
                text = "Row Item : $it",
                modifier = Modifier
                    .padding(horizontal = 8.dp)
                    .background(Color.Green)
            )
            Spacer(modifier = Modifier.width(8.dp))
        }
    }
}
```

![LazyRow](../assets/LazyRow.gif)

## 视频教程

- <a href="https://www.bilibili.com/video/BV1RP4y1u7q8/">https://www.bilibili.com/video/BV1RP4y1u7q8/</a>
- <a href="https://www.bilibili.com/video/BV1aS4y1D7dv/">实战视频</a>