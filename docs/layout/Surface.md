``` kotlin
@Composable
@NonRestartableComposable
fun Surface(
    modifier: Modifier? = Modifier,
    shape: Shape? = RectangleShape,
    color: Color? = MaterialTheme.colors.surface,
    contentColor: Color? = contentColorFor(color),
    border: BorderStroke? = null,
    elevation: Dp? = 0.dp,
    content: (@Composable () -> Unit)?
): Unit
```

- shape 设置形状：RectangleShape、CircleShape、RoundedCornerShape、CutCornerShape
- border
- elevation

``` kotlin
@Composable
fun SurfaceSample() {
    Column() {
        Surface(
            modifier = Modifier
                .height(100.dp)
                .padding(10.dp),
            color = Color.Yellow,
            shape = CutCornerShape(10.dp),
            border = BorderStroke(1.dp, Color.Green),
            elevation = 10.dp
        ) {
            Image(
                painter = painterResource(id = R.drawable.newbanner4),
                contentDescription = null,
            )
        }
    }
}
```

![Surface](../assets/Surface.png)

## 视频教程

- <a href="https://www.bilibili.com/video/BV1CT4y1Q7de/">https://www.bilibili.com/video/BV1CT4y1Q7de/</a>
- <a href="https://www.bilibili.com/video/BV1aS4y1D7dv/">实战视频</a>