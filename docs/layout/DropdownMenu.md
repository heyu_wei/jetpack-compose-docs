下拉列表菜单

``` kotlin
@Composable
fun DropdownMenu(
    expanded: Boolean?,
    onDismissRequest: (() -> Unit)?,
    modifier: Modifier? = Modifier,
    offset: DpOffset? = DpOffset(0.dp, 0.dp),
    properties: PopupProperties? = PopupProperties(focusable = true),
    content: (@Composable @ExtensionFunctionType ColumnScope.() -> Unit)?
): Unit
```

- expanded 是否展开
- onDismissRequest 取消回调
- offset 设置偏移量
- properties 可以设置 focusable、dismissOnBackPress、dismissOnClickOutside、securePolicy

``` kotlin
@Composable
fun DropdownMenuSample() {

    var expanded by remember {
        mutableStateOf(false)
    }

    Column() {
        IconButton(onClick = {
            expanded = !expanded
        }) {
            Icon(imageVector = Icons.Default.PinDrop, contentDescription = null)
        }
        DropdownMenu(
            expanded = expanded, onDismissRequest = {
                Log.i("======", "==========")
                expanded = false
            }, offset = DpOffset(x = 10.dp, y = 10.dp),
            properties = PopupProperties(
                focusable = true,
                dismissOnBackPress = false,
                dismissOnClickOutside = false,
                securePolicy = SecureFlagPolicy.SecureOn,//设置安全级别，是否可以截屏
            )
        ) {
            DropdownMenuItem(onClick = { expanded = false }) {
                Text(text = "Menu 0")
            }

            DropdownMenuItem(onClick = { expanded = false }) {
                Text(text = "Menu 1")
            }

            DropdownMenuItem(onClick = { expanded = false }) {
                Text(text = "Menu 2")
            }
        }
    }
}
```

![DrowdownMenu](../assets/DrowdownMenu.gif)

## 视频教程

- <a href="https://www.bilibili.com/video/BV1QL4y1M7EA/">https://www.bilibili.com/video/BV1QL4y1M7EA/</a>
- <a href="https://www.bilibili.com/video/BV1aS4y1D7dv/">实战视频</a>