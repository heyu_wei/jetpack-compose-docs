## 属性

``` kotlin
@ExperimentalFoundationApi
@Composable
fun LazyVerticalGrid(
    cells: GridCells?,
    modifier: Modifier? = Modifier,
    state: LazyGridState? = rememberLazyGridState(),
    contentPadding: PaddingValues? = PaddingValues(0.dp),
    verticalArrangement: Arrangement.Vertical? = Arrangement.Top,
    horizontalArrangement: Arrangement.Horizontal? = Arrangement.Start,
    flingBehavior: FlingBehavior? = ScrollableDefaults.flingBehavior(),
    userScrollEnabled: Boolean? = true,
    content: (@ExtensionFunctionType LazyGridScope.() -> Unit)?
): Unit
```

## 参数

- Cells 描述单元格展现形式

  GridCells.Adaptive：设置单元格的最小尺寸，让内容自适应

  GridCells.Fixed：设置单元格的固定尺寸

``` kotlin
@OptIn(ExperimentalFoundationApi::class)
@Composable
fun LazyVerticalGridSample() {
    val datas = listOf(1, 2, 3, 4, 5, 6, 7, 8, 9, 10)
    LazyVerticalGrid(cells = GridCells.Fixed(2)) {
        items(datas) {
            Text("Grid Item $it")
        }
    }
}
```

![LazyVerticalGrid](../assets/LazyVerticalGrid.png)

## 视频教程

- <a href="https://www.bilibili.com/video/BV1Mi4y117io/">https://www.bilibili.com/video/BV1Mi4y117io/</a>
- <a href="https://www.bilibili.com/video/BV1aS4y1D7dv/">实战视频</a>