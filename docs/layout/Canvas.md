

## DrawScope

- drawLine 画直线
  - color 设置画笔颜色 
  - start 开始坐标
  - end 结束坐标
  - cap 设置线段头尾样式，StrokeCap.Round为圆形
- drawRect 画矩形
  - color 画笔颜色 
  - size 矩形大小
  - topLeft 偏移左上角的距离
- drawImage 画图片
  - topLeft 偏移左上角的距离
  - colorFilter 设置颜色滤镜
  - blendMode 渲染模式
- drawRoundRect 画圆角矩形
  - cornerRadius 圆角大小
- drawCircle 画圆形
  - style 
- drawOval 画椭圆
- drawArc 画扇形
  - startAngle 开始角度
  - sweepAngle 展开角度
  - useCenter 是否填充
- drawPoints 画点
  - pointMode 各种之间的模式，PointMode.Points为加点；Lines为每两点间连线；Polygon所有点连在一起
- drawPath 根据路径绘制

``` kotlin
@Composable
fun CanvasSample() {
    var imageBitmap: ImageBitmap? = null
    with(LocalContext.current) {
        imageBitmap = ImageBitmap.imageResource(id = R.drawable.newbanner4)
    }
    Canvas(
        modifier = Modifier
            .size(200.dp)
            .background(Color.LightGray)
    ) {


        drawLine(
            Color.Yellow,
            start = Offset(x = 0f, y = 10f),
            end = Offset(x = 100f, y = 200f),
            strokeWidth = 10f,
            cap = StrokeCap.Round
        )

        drawRect(Color.Yellow, size = Size(100f, 100f), topLeft = Offset(x = 10f, y = 0f))

        imageBitmap?.let { drawImage(it) }

        drawRoundRect(
            Color.Green,
            size = Size(100f, 100f),
            cornerRadius = CornerRadius(20f, 20f),
            style = Stroke(width = 10f)
        )

        drawCircle(Color.Red, style = Stroke(width = 10f))

        drawOval(Color.Cyan)

        drawArc(
            Color.Blue,
            startAngle = 0f,
            sweepAngle = 30f,
            useCenter = false,
            style = Stroke(width = 10f)
        )

        drawPoints(
            listOf(Offset(10f, 10f), Offset(20f, 50f), Offset(40f, 60f), Offset(60f, 100f)),
            pointMode = PointMode.Lines,
            Color.Black,
            strokeWidth = 10f,
            cap = StrokeCap.Round
        )

    }
}
```

![Canvas](../assets/Canvas.png)

## 视频教程

- <a href="https://www.bilibili.com/video/BV1mU4y1f7XX/">https://www.bilibili.com/video/BV1mU4y1f7XX/</a>
- <a href="https://www.bilibili.com/video/BV1aS4y1D7dv/">实战视频</a>