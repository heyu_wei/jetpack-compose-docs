``` kotlin
@Composable
fun Dialog(
    onDismissRequest: () -> Unit,
    properties: DialogProperties = DialogProperties(),
    content: @Composable () -> Unit
):Unit
```

## Dialog 用法

``` kotlin
@Composable
fun DialogSample() {
    var showDialog by remember {
        mutableStateOf(false)
    }
    Column() {
        Button(onClick = { showDialog = !showDialog }) {
            Text("show dialog")
        }
        if (showDialog) {
            Dialog(onDismissRequest = { showDialog = !showDialog }) {
                Box(
                    Modifier
                        .size(200.dp, 50.dp)
                        .background(Color.White))
            }
        }
    }

}
```

![Dialog](../assets/Dialog.gif)



## AlertDialog 用法

``` kotlin
@Composable
fun AlertDialogSample() {

    var showDialog by remember {
        mutableStateOf(false)
    }

    Column() {
        Button(onClick = { showDialog = !showDialog }) {
            Text("show dialog")
        }
        if (showDialog) {
            AlertDialog(
                onDismissRequest = {
                    // Dismiss the dialog when the user clicks outside the dialog or on the back
                    // button. If you want to disable that functionality, simply use an empty
                    // onCloseRequest.
                    showDialog = false
                },
                title = {
                    Text(text = "Title")
                },
                text = {
                    Text(
                        "This area typically contains the supportive text " +
                                "which presents the details regarding the Dialog's purpose."
                    )
                },
                confirmButton = {
                    TextButton(
                        onClick = {
                            showDialog = false
                        }
                    ) {
                        Text("Confirm")
                    }
                },
                dismissButton = {
                    TextButton(
                        onClick = {
                            showDialog = false
                        }
                    ) {
                        Text("Dismiss")
                    }
                }
            )
        }
    }

}
```

![AlertDialog](../assets/AlertDialog.gif)

## 视频教程

- <a href="https://www.bilibili.com/video/BV1tR4y1G7wR/">https://www.bilibili.com/video/BV1tR4y1G7wR/</a>
- <a href="https://www.bilibili.com/video/BV1aS4y1D7dv/">实战视频</a>