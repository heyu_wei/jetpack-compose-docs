## 属性

``` kotlin
@Composable
inline fun Row(
    modifier: Modifier? = Modifier,
    horizontalArrangement: Arrangement.Horizontal? = Arrangement.Start,
    verticalAlignment: Alignment.Vertical? = Alignment.Top,
    content: (@Composable @ExtensionFunctionType RowScope.() -> Unit)?
): Unit
```

Row 布局可以让其子组件呈横向排列，在 RowScope 中可以使用 Modifier.weight 来设置子组件所占比重。

## 参数

- horizontalArrangement 设置横向排列方式

  ![row_arrangement_visualization](../assets/row_arrangement_visualization.gif)

- verticalAlignment 设置纵向对齐方式

``` kotlin
@Composable
fun RowSample() {
    Row(
        modifier = Modifier
            .width(100.dp)
            .background(Color.White)
    ) {
        Text("First", modifier = Modifier.background(Color.Red))

        Text("Second", modifier = Modifier.background(Color.Green))
    }
}
```

## 视频教程

<a href="https://www.bilibili.com/video/BV1Su411Q7FB" target="_blank">https://www.bilibili.com/video/BV1Su411Q7FB</a>

<a href="https://www.bilibili.com/video/BV1aS4y1D7dv/">实战视频</a>