## 属性

``` kotlin
@Composable
fun LazyColumn(
    modifier: Modifier? = Modifier,
    state: LazyListState? = rememberLazyListState(),
    contentPadding: PaddingValues? = PaddingValues(0.dp),
    reverseLayout: Boolean? = false,
    verticalArrangement: Arrangement.Vertical? = if (!reverseLayout) Arrangement.Top else Arrangement.Bottom,
    horizontalAlignment: Alignment.Horizontal? = Alignment.Start,
    flingBehavior: FlingBehavior? = ScrollableDefaults.flingBehavior(),
    userScrollEnabled: Boolean? = true,
    content: (@ExtensionFunctionType LazyListScope.() -> Unit)?
): Unit
```

可滚动的带回收机制的 Column

## 参数

- state 管理列表的状态，可通过这个操作列表滚动

在 LazyListScpoe 中，可以使用以下语句进行操作：

- item 添加单个列表项
- items 添加一组列表项
- itemsIndexed 
- stickyHeader 添加粘性头部

``` kotlin
@OptIn(ExperimentalFoundationApi::class)
@Composable
fun LazyColumnSample1() {
    val items = listOf(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20)

    var height: Dp

    with(LocalDensity.current) {
        height = 300.toDp()
    }

    val state = rememberLazyListState()

    val scope = rememberCoroutineScope()

    LazyColumn(state = state) {
        stickyHeader {
            Text(
                "Header", modifier = Modifier
                    .fillMaxWidth()
                    .background(Color.White)
                    .padding(vertical = 10.dp)
                    .clickable {
                        scope.launch { state.animateScrollToItem(2) }
                    }
            )
        }
        items(items) {
            Text(
                text = "Column Item :$it", modifier = Modifier
                    .height(height)
            )
            Divider()
            DisposableEffect(Unit) {
                Log.d("====", "effect:$it")
                onDispose {
                    Log.e("====", "onDispose:$it")
                }
            }
        }
    }
}
```

![LazyColumn](../assets/LazyColumn.gif)

## 视频教程

<a href="https://www.bilibili.com/video/BV12Y411G71N">https://www.bilibili.com/video/BV12Y411G71N</a>

<a href="https://www.bilibili.com/video/BV1aS4y1D7dv/">实战视频</a>