## 如果你有的话

在学习 Jetpack Compose 之前，建议最好准备梯子，因为首先第一步下载Android Studio 就需要爬梯子，包括后续的查看官方文档的过程中也是需要梯子的。

当然没有的情况下也不是不能学习，只是可能过程会比较困难

## 下载 Android Studio

<a href="https://developer.android.com/studio" target="_blank">官方下载地址</a>

点击上面的地址直达页面下载最新版本的，如果电脑上已经安装了 Android Studio，一定要确定版本是```Arctic Fox | 2020.3.1```之后的版本（包含）。

点击 Download Android Sudio

![First](../assets/download1.png)

勾选同意协议，点击 Download Android Studio for Windows

![Second](../assets/download_windows.png)

### 网盘下载

<a href="https://www.aliyundrive.com/s/EFF9jEWQNWu" target="_blank">网盘下载</a>

网盘里面有两个版本，大家可以看时间下载最后的版本即可

## 安装 Android Studio

双击下载好的 exe 文件，按照步骤选择安装目录进行安装

## 视频教程

<a href="https://www.bilibili.com/video/BV19i4y117yB/?spm_id_from=333.788">https://www.bilibili.com/video/BV19i4y117yB/?spm_id_from=333.788</a>

<a href="https://www.bilibili.com/video/BV1aS4y1D7dv/">实战视频</a>